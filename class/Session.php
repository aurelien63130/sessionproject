<?php
class Session{
    public $attributs = [];
    public $test = 'Hello';

    public function addOrUpdate($key, $value){
        $this->attributs[$key] = $value;
    }

    public function __construct(){
       if(isset($_SESSION['session'])){
           $session = unserialize($_SESSION['session']);
           $this->attributs = $session->attributs;
       }
    }

    public function __get($key){
        return $this->attributs[$key];
    }

    public function __set($key, $value){
        $this->addOrUpdate($key, $value);
    }

    public function __isset($key){
        return isset($this->attributs[$key]);
    }

    public function __unset($key){
        unset($this->attributs[$key]);
    }

    public function __destruct(){
        $_SESSION['session'] = serialize($this);
    }

    public function printSession(){
        foreach ($this->attributs as $key=>$val){
            echo('Clé : '.$key.' => Valeur :'.$val .'<br>');
        }
    }

    public function logout(){
        $this->attributs = [];
        session_destroy();
    }


}